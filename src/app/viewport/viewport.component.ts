import { Component, OnInit } from '@angular/core';
import { ShaderUtils } from '../shader-utils';
import { ShaderSources } from '../shader-sources';
import { Renderer } from '../renderer';

@Component({
  selector: 'app-viewport',
  templateUrl: './viewport.component.html',
  styleUrls: ['./viewport.component.scss']
})
export class ViewportComponent implements OnInit {

  canvas: HTMLCanvasElement;
  gl: WebGLRenderingContext;
  shaderProgram: WebGLProgram;
  renderer: Renderer;

  constructor() {}

  ngOnInit() {
    this.canvas = document.querySelector('#viewportCanvas');
    this.gl = this.getRenderingContext(this.canvas);

    if (this.gl != null) {
      this.initialiseContext();
    } else {
      return;
    }

    this.shaderProgram = ShaderUtils.createShaderProgram(this.gl, ShaderSources.vertexSource, ShaderSources.fragmentSource);

    const programInfo = {
      program: this.shaderProgram,
      attributeLocations: {
        vertexPositions: this.gl.getAttribLocation(this.shaderProgram, 'aVertexPosition'),
        vertexColor: this.gl.getAttribLocation(this.shaderProgram, 'aVertexColor'),
      },
      uniformLocations: {
        projectionMatrix: this.gl.getUniformLocation(this.shaderProgram, 'uProjectionMatrix'),
        modelViewMatrix: this.gl.getUniformLocation(this.shaderProgram, 'uModelViewMatrix'),
      },

    };

    this.renderer = new Renderer(this.gl);
    const buffer = this.initBuffers(this.gl);
    this.renderer.drawScene(programInfo, buffer);

  }

  private initialiseContext(): void {
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT);
  }

  private getRenderingContext(canvas: HTMLCanvasElement): WebGLRenderingContext {
    const context = canvas.getContext('webgl');
    if (!context) {
      console.error('Failed to initialise WebGL. This browser doesn\'t support it.');
      return null;
    } else {
      return context;
    }
  }

  private initBuffers(gl: WebGLRenderingContext): WebGLBuffer {
    
    const positions = [
      1.0,  1.0,
     -1.0,  1.0,
      1.0, -1.0,
     -1.0, -1.0,
    ];
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER,
    new Float32Array(positions), gl.STATIC_DRAW);


    const colors = [
     1.0, 1.0, 1.0, 1.0,  // white
     1.0, 0.0, 0.0, 1.0,  // red
     0.0, 1.0, 0.0, 1.0,  // green
     0.0, 0.0, 1.0, 1.0,  // blue
    ];
    const colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    return {
      position: positionBuffer,
      color: colorBuffer,
    };
  }

}
