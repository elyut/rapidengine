import { mat4 } from 'gl-matrix';

export class Renderer {
  public constructor(
    private gl: WebGLRenderingContext,
    public fov: number = 45 * Math.PI / 180,
    public aspectRatio: number = gl.canvas.clientWidth / gl.canvas.clientHeight,
    public zNear: number = 0.1,
    public zFar: number = 100.0,
    public projectionMatrix: mat4 = mat4.create()
  ) {}

  public drawScene(programInfo, buffers) {

    this.resetScene();

    mat4.perspective(
      this.projectionMatrix,
      this.fov,
      this.aspectRatio,
      this.zNear,
      this.zFar
    );

    const modelViewMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [-0.0, 0.0, -6.0]);

    {
        const numComponents = 2;
        const type = this.gl.FLOAT;
        const normalise = false;
        const stride = 0;
        const offset = 0;
        
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffers.position);
        this.gl.vertexAttribPointer(
            programInfo.attributeLocations.vertexPosition,
            numComponents,
            type,
            normalise,
            stride,
            offset
        );
        this.gl.enableVertexAttribArray(
            programInfo.attributeLocations.vertexPosition
        );
    }

    {
        const numComponents = 4;
        const type = this.gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffers.color);
        this.gl.vertexAttribPointer(
            programInfo.attributeLocations.vertexColor,
            numComponents,
            type,
            normalize,
            stride,
            offset
        );
        this.gl.enableVertexAttribArray(
            programInfo.attributeLocations.vertexColor
        );
    }


    this.gl.useProgram(programInfo.program);

    this.gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        this.projectionMatrix
    );

    this.gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix
    );

    {
        const offset = 0;
        const vertexCount = 4;
        this.gl.drawArrays(this.gl.TRIANGLE_STRIP, offset, vertexCount);
    }
  }

  private resetScene(): void {
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    this.gl.clearDepth(1.0);                 // Clear everything
    this.gl.enable(this.gl.DEPTH_TEST);           // Enable depth testing
    this.gl.depthFunc(this.gl.LEQUAL);            // Near things obscure far things

    // Clear the canvas before we start drawing on it.

    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
  }
}
