export class ShaderUtils {

    public static createShaderProgram(gl : WebGLRenderingContext, vertexSource : string, fragmentSource : string) : WebGLProgram {
        const vertexShader = this.loadShader(gl, gl.VERTEX_SHADER, vertexSource);
        const fragmentShader = this.loadShader(gl, gl.FRAGMENT_SHADER, fragmentSource);

        // Create the shader program
        const shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        // Check the program creation
        if(!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert(`Unable to initialise the shader program: ${gl.getProgramInfoLog(shaderProgram)}`);
            return null;
        }

        return shaderProgram;
    }

    /**
     * Creates a shader of a given type, uploads the source code for it and then compiles it.
     * 
     * @param gl The WebGL Rendering Context used to create the shader program.
     * @param type The type of shader to create.
     * @param source The source code for the shader.
     * 
     * @returns The resulting shader or null if an error occurred.
     */
    public static loadShader(gl : WebGLRenderingContext, type : number, source : string) : WebGLShader {
        const shader = gl.createShader(type);

        // Send the shader source to the shader object.
        gl.shaderSource(shader, source);

        // Compile the shader program.
        gl.compileShader(shader);

        // Check the compilation
        if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            alert(`An error occurred compiling the shader: ${gl.getShaderInfoLog(shader)}`);
            gl.deleteShader(shader);
            return null;
        }

        return shader;
    }
}
